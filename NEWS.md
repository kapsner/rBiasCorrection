# rBiasCorrection NEWS

## Unreleased (2022-06-20)

#### Refactorings

* removing deprecated future-multiprocess
#### Docs

* updated vignette
* updated url to cran hosted vignette
#### Others

* added dependencies badge
* updated lintr
* added news.md
* updating package code, badges, gha
* fixed test errors (linting errors)

Full set of changes: [`v0.3.3...faa1eb7`](https://github.com/kapsner/rBiasCorrection/compare/v0.3.3...faa1eb7)

## v0.3.3 (2022-02-16)


Full set of changes: [`v0.3.2...v0.3.3`](https://github.com/kapsner/rBiasCorrection/compare/v0.3.2...v0.3.3)

## v0.3.2 (2021-08-03)

#### Fixes

* unit tests; update to 0.3.2

Full set of changes: [`v0.3.1...v0.3.2`](https://github.com/kapsner/rBiasCorrection/compare/v0.3.1...v0.3.2)

## v0.3.1 (2021-06-21)


Full set of changes: [`v0.3.0...v0.3.1`](https://github.com/kapsner/rBiasCorrection/compare/v0.3.0...v0.3.1)

## v0.3.0 (2021-05-17)


Full set of changes: [`v0.2.9...v0.3.0`](https://github.com/kapsner/rBiasCorrection/compare/v0.2.9...v0.3.0)

## v0.2.9 (2021-03-29)


Full set of changes: [`v0.2.8...v0.2.9`](https://github.com/kapsner/rBiasCorrection/compare/v0.2.8...v0.2.9)

## v0.2.8 (2021-03-27)


Full set of changes: [`v0.2.7...v0.2.8`](https://github.com/kapsner/rBiasCorrection/compare/v0.2.7...v0.2.8)

## v0.2.7 (2021-03-01)


Full set of changes: [`v0.2.6...v0.2.7`](https://github.com/kapsner/rBiasCorrection/compare/v0.2.6...v0.2.7)

## v0.2.6 (2021-02-13)


Full set of changes: [`v0.2.5...v0.2.6`](https://github.com/kapsner/rBiasCorrection/compare/v0.2.5...v0.2.6)

## v0.2.5 (2020-12-17)


Full set of changes: [`v0.2.4...v0.2.5`](https://github.com/kapsner/rBiasCorrection/compare/v0.2.4...v0.2.5)

## v0.2.4 (2020-11-16)


Full set of changes: [`v0.2.3...v0.2.4`](https://github.com/kapsner/rBiasCorrection/compare/v0.2.3...v0.2.4)

## v0.2.3 (2020-09-22)


Full set of changes: [`v0.2.2...v0.2.3`](https://github.com/kapsner/rBiasCorrection/compare/v0.2.2...v0.2.3)

## v0.2.2 (2020-09-13)


Full set of changes: [`v0.2.1...v0.2.2`](https://github.com/kapsner/rBiasCorrection/compare/v0.2.1...v0.2.2)

## v0.2.1 (2020-07-22)


Full set of changes: [`v0.2.0...v0.2.1`](https://github.com/kapsner/rBiasCorrection/compare/v0.2.0...v0.2.1)

## v0.2.0 (2020-07-17)


Full set of changes: [`v0.1.7...v0.2.0`](https://github.com/kapsner/rBiasCorrection/compare/v0.1.7...v0.2.0)

## v0.1.7 (2020-06-17)


Full set of changes: [`v0.1.6...v0.1.7`](https://github.com/kapsner/rBiasCorrection/compare/v0.1.6...v0.1.7)

## v0.1.6 (2020-01-18)


Full set of changes: [`v0.1.5...v0.1.6`](https://github.com/kapsner/rBiasCorrection/compare/v0.1.5...v0.1.6)

## v0.1.5 (2019-12-16)


Full set of changes: [`v0.1.4...v0.1.5`](https://github.com/kapsner/rBiasCorrection/compare/v0.1.4...v0.1.5)

## v0.1.4 (2019-11-27)


Full set of changes: [`v0.1.3...v0.1.4`](https://github.com/kapsner/rBiasCorrection/compare/v0.1.3...v0.1.4)

## v0.1.3 (2019-11-09)

